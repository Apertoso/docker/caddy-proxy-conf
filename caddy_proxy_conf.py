#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import logging
import multiprocessing
import os
import signal
import socket
import time

import docker
import jinja2
import requests

_logger = logging.getLogger(__name__)

PROXY_CONF_FILE = "/srv/caddy/Caddyfile"
TEMPLATES_PATH = "/templates/"
PROXY_RELOAD_SIGNAL = signal.SIGUSR1
PROXY_CONTAINER_FILTER = {'label': 'caddy_container', 'status': 'running', }

PUBLIC_SERVICE_IP = os.environ.get('PUBLIC_SERVICE_IP', False)
WILDCARD_CERTS = os.environ.get('WILDCARD_CERTS', '')


class ContainerNotFoundException(Exception):
    pass


def get_local_ip():
    if PUBLIC_SERVICE_IP:
        return PUBLIC_SERVICE_IP
    else:
        req = requests.get('https://httpbin.org/ip')
        req.raise_for_status()
        local_ip = req.json().get('origin', None)
        return local_ip


def check_hostname_ip(local_ip, hostname):
    try:
        hostname_ip = socket.gethostbyname(hostname)
    except socket.gaierror:
        _logger.exception(
            'Exception resolving hostname %s' % hostname
        )
        hostname_ip = False
    if hostname_ip != local_ip:
        _logger.debug(
            'Hostname %s resolves to %s, instead of %s' %
            (hostname, hostname_ip, local_ip)
        )
        return False
    else:
        return True


def check_wildcard_certs(domain):
    for wc_cert in WILDCARD_CERTS.split(','):
        domain_parts = domain.split('.')[1:]
        wc_cert_parts = wc_cert.split('.')[1:]
        if wc_cert_parts == domain_parts:
            return '.'.join(wc_cert_parts)


def get_wildcard_certinfo():
    return {
        'wildcard_certificates': [
            '.'.join(c.split('.')[1:]) for c in WILDCARD_CERTS.split(',')
        ]
    }


class WorkQueue(object):
    def __init__(self, call_function=None, delay=10):
        self.event = multiprocessing.Event()
        self.delay = delay
        self.call_function = call_function
        self.process = multiprocessing.Process(
            target=self.run,
        )
        self.process.daemon = True

    def run(self):
        while True:
            _logger.debug('waiting for signal')
            self.event.wait()
            _logger.debug('recieved signal, waiting')
            time.sleep(self.delay)
            _logger.debug('running func')
            self.call_function()
            self.event.clear()

    def signal(self):
        _logger.debug('Send signal')
        self.event.set()

    def start(self):
        _logger.debug('Starting up')
        self.process.start()


class ProxyCertbotConfigurator(object):
    def __init__(self):
        self.docker_client = docker.from_env()
        self.delay = 10
        self.local_ip = get_local_ip()
        _logger.warn('Local ip has been set to %s' % self.local_ip)

        self.template_loader = jinja2.Environment(
            loader=jinja2.FileSystemLoader(TEMPLATES_PATH)
        )
        self.proxy_queue = WorkQueue(
            call_function=self.renew_proxy_config,
            delay=self.delay
        )
        self.proxy_queue.start()

    def get_vhost_template(self, template):
        template_name = '%s-vhost.conf.j2' % template
        return self.template_loader.get_template(template_name)

    def get_containers_info(self):

        vhosts_map = {}

        for container_obj in self.docker_client.containers.list(
                filters={
                    'label': 'vhost_primary_domain',
                    'status': 'running',
                }):
            container = container_obj.attrs
            labels = container['Config']['Labels']
            primary_domain = labels.get(
                'vhost_primary_domain')
            if labels.get('vhost_redirect_domains'):
                redirect_domains = map(
                    lambda s: s.strip(),
                    labels.get('vhost_redirect_domains').split(',')
                )
            else:
                redirect_domains = []
            if labels.get('vhost_extra_domains'):
                extra_domains = map(
                    lambda s: s.strip(),
                    labels.get('vhost_extra_domains').split(',')
                )
            else:
                extra_domains = []
            path = labels.get(
                'vhost_path', '/')
            port = labels.get(
                'vhost_backend_port', False)
            template = labels.get(
                'vhost_template', 'odoo')
            odoo_dbfilter = labels.get(
                'vhost_odoo_dbfilter', False)

            # find the network we need ( the user defined one )
            # these seem to have aliases ....
            container_networks = filter(
                lambda n: bool(n.get('Aliases')),
                container['NetworkSettings']['Networks'].values()
            )
            if len(container_networks) == 1:
                container_ip = container_networks[0].get('IPAddress')
            else:
                container_ip = None

            if not container_ip:
                _logger.warn(
                    'Cannot find the right docker network for container %s.  '
                    'Vhost for this container will not be configured' %
                    container['Name']
                )
                continue

            domains = [primary_domain] + extra_domains

            # For each of the domains, generate configuration
            for domain in domains:

                if not domain:
                    continue

                # does our domain matches a site wide wildcard certificate ?
                match_wildcard_domain = check_wildcard_certs(domain)
                # or does our domain is its own wildcard record ?
                domain_is_wildcard = domain[0:2] == '*.'

                if not (match_wildcard_domain or
                        domain_is_wildcard or
                        check_hostname_ip(self.local_ip, domain)):
                    _logger.error(
                        'Domain name %s does not resolve to our local IP %s, '
                        'skipping configuration for domain %s' % (
                            domain, self.local_ip, domain
                        )
                    )
                    continue

                vhost_data = vhosts_map.get(domain, {})
                endpoints = vhost_data.get('endpoints', [])
                endpoints.append(container_ip)
                vhost_data.update({
                    'domain': domain,
                    'match_wildcard_domain': match_wildcard_domain,
                    'domain_is_wildcard': domain_is_wildcard,
                    'endpoints': endpoints,
                    'odoo_dbfilter': odoo_dbfilter,
                    'port': port,
                    'path': path.rstrip('/'),
                    'template': template,
                })
                # only configure redirects for the primary domain
                if domain == primary_domain:
                    vhost_data.update({
                        'redirect_domains': redirect_domains,
                    })
                vhosts_map.update({domain: vhost_data})

        return vhosts_map.values()

    def render_templates(self, contexts):

        with open(PROXY_CONF_FILE + '.head', 'r') as config_head_fh:
            config_header = config_head_fh.read()
        with open(PROXY_CONF_FILE, 'w') as config_fileh:
            config_fileh.write(config_header)

            config_fileh.write(
                self.get_vhost_template(
                    'wildcardcerts'
                ).render(get_wildcard_certinfo())
            )

            for context in contexts:
                _logger.debug(
                    'Rendering template for domain "%s"' % context['domain']
                )
                template = context.get('template')
                jinja_template = self.get_vhost_template(template)
                config_fileh.write(jinja_template.render(context))

    def signal_proxy_container(self, send_signal=PROXY_RELOAD_SIGNAL):
        for container in self.docker_client.containers.list(
                filters=PROXY_CONTAINER_FILTER):
            container.kill(signal=send_signal)

    def renew_proxy_config(self):
        _logger.info('Renewing proxy config')
        try:
            self.render_templates(self.get_containers_info())
            self.signal_proxy_container()
        except:
            _logger.exception('Exception in renew_proxy_config')

    def listen_docker(self):
        # generate an initial proxy conf at startup
        for event_str in self.docker_client.events():
            event = json.loads(str(event_str))
            ev_type = event.get('Type')
            ev_act = event.get('Action')
            if ev_type not in ('container'):
                continue
            if ev_act not in ('start', 'stop', 'die'):
                continue
            attributes = event.get('Actor', {}).get('Attributes', {})
            if 'vhost_primary_domain' not in attributes.keys():
                continue

            _logger.debug('received event %s' % event_str)
            _logger.info('signaling proxy')
            self.proxy_queue.signal()

    def start(self):

        # Generate config and certs for already running containers
        self.proxy_queue.signal()

        # Main process: watch for docker events,
        self.listen_docker()


if __name__ == '__main__':
    root_logger = logging.getLogger()
    ch = logging.StreamHandler()
    root_logger.setLevel(logging.INFO)
    # urllib3 = logging.getLogger('urllib3')
    # urllib3.setLevel(logging.INFO)
    root_logger.addHandler(ch)
    config_obj = ProxyCertbotConfigurator()
    config_obj.start()
